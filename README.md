# Cubes3D

This repository contains scripts to generate synthetic ground truth flow data.

## Requirements:
For `generateOpenEXR.py`, you need
 * Blender (we used version 2.79)
 
For `exr2flo_png.py`, you need the Python 3 modules
 * numpy
 * OpenEXR
 * PIL
 
## Running the code

To generate ***.exr** files containing the flow and the image, execute
```
PYTHONPATH=$PYTHONPATH:'' blender -b -P generateOpenEXR.py OUTPUT_DIR
```
The files are written to *OUTPUT_DIR*.

**Note:** `PYTHONPATH` needs to be set otherwise local imports cannot be found.


To create separate files for flow and image from the previously generated
***.exr** files, run
```
./exr2flo_png.py OUTPUT_DIR
```
The extracted files are written to *OUTPUT_DIR/ground_truth* and *OUTPUT_DIR/images*.


## Evaluation

To calculate the percentage of outliers over foreground and background
you have to pass the input image and the background image of your scene
to `mask()` and that result becomes the third input of `compareFiles()`.
