#!/usr/bin/env python3

import bpy
import os
import sys

from functions import *

# USER INPUT
speed = 1 # 1, 2 or 4

## Camera
camera_location = (0, 0, 2.5)
camera_rotation = (0, 0, 0)

## Cube
cube_color_1 = (0, 1, 0)
cube_color_2 = (1, 0, 0)
### use checkerboard texture (if `false` only `cube_color_1` is used)
cube_with_texture = True
### path
line = []
line.append((-15, 10, 0, 1.0))
line.append((-7.5, 10, 0, 1.0))
line.append((0, 10, 0, 1.0))
line.append((7.5, 10, 0, 1.0))
line.append((15, 10, 0, 1.0))

linec = []
linec.append((-15, 0, 0, 1.0))
linec.append((-7.5, 0, 0, 1.0))
linec.append((0, 0, 0, 1.0))
linec.append((7.5, 0, 0, 1.0))
linec.append((15, 0, 0, 1.0))

spiral = []
spiral.append((0, 0, 0, 1.0))
spiral.append((15, 15, 0, 1.0))
spiral.append((15, -15, 0, 1.0))
spiral.append((-15, -15, 0, 1.0))
spiral.append((-15, 15, 0, 1.0))

# use one of line, linec or spiral for cube_path
cube_path = line

## Light
light_location = (0, 0, 2.5)

## Image
target_img_h = 1680
target_img_w = 1680
# resolution percentage
res_per = 100 # use a lower value for test purposes to improve render speed


if len(sys.argv) < 5:
    exit("Last argument needs to be a directory.")

target_dir = os.path.abspath(sys.argv[-1])
if not os.path.exists(target_dir):
    os.makedirs(target_dir, exist_ok=True)

if not os.path.isdir(target_dir):
    exit("{} is not a directory".format(target_dir))


max_frames = 200/speed

# init blender project
delete_all()

scn = bpy.context.scene
scn.world.use_nodes = True
scn.render.engine = 'CYCLES'
scn.unit_settings.system = 'METRIC'

scn.camera = add_fisheye_camera(camera_location, camera_rotation)

# add a lamp
bpy.ops.object.lamp_add(type='POINT', radius=1, view_align=False, location=light_location,
    layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
bpy.context.object.data.shadow_soft_size = 0.01
bpy.context.object.data.cycles.cast_shadow = True

target_objects = []

# add path
bpy.ops.curve.primitive_nurbs_path_add()
path = bpy.context.object

path.data.path_duration = max_frames

bpy.ops.object.mode_set(mode = 'EDIT')

init_path_length = len(path.data.splines[0].points)
path_length = len(cube_path)
if path_length > init_path_length:
    path.data.splines[0].points.add(path_length-init_path_length)

for i in range(path_length):
    path.data.splines[0].points[i].co = cube_path[i]

bpy.ops.object.mode_set(mode = 'OBJECT')

# add cube
bpy.ops.mesh.primitive_cube_add()
cube = bpy.context.object
material = bpy.data.materials.new(name="Material")
if cube_with_texture:
    material.use_nodes = True
    checker = material.node_tree.nodes.new('ShaderNodeTexChecker')
    checker.inputs['Color1'].default_value = (*cube_color_1, 1)
    checker.inputs['Color2'].default_value = (*cube_color_2, 1)
    bsdf = material.node_tree.nodes['Diffuse BSDF']
    material.node_tree.links.new(checker.outputs['Color'], bsdf.inputs['Color'])
else:
    material.diffuse_color = cube_color_1

if cube.data.materials:
    cube.data.materials[0] = material
else:
    cube.data.materials.append(material)

cube.constraints.new(type='FOLLOW_PATH')
cube.constraints["Follow Path"].target = path
cube.constraints["Follow Path"].use_curve_follow = True
override={'constraint':cube.constraints["Follow Path"]}
bpy.ops.constraint.followpath_path_animate(override,constraint='Follow Path')


# set the rendering parameter
scn.render.resolution_x = target_img_w
scn.render.resolution_y = target_img_h
scn.render.resolution_percentage = res_per
scn.render.pixel_aspect_x = 1
scn.render.pixel_aspect_y = 1
## add the file format extensions to the rendered file name
scn.render.use_file_extension = True
scn.render.image_settings.color_mode ='RGB'
scn.render.image_settings.compression = 90

scn.cycles.device = 'GPU'
scn.cycles.progressive = 'PATH'
scn.cycles.samples = 50
scn.cycles.max_bounces = 1
scn.cycles.min_bounces = 1
scn.cycles.glossy_bounces = 1
scn.cycles.transmission_bounces = 1
scn.cycles.volume_bounces = 1
scn.cycles.transparent_max_bounces = 1
scn.cycles.transparent_min_bounces = 1
scn.cycles.use_progressive_refine = True
scn.render.tile_x = 64
scn.render.tile_y = 64
scn.render.layers['RenderLayer'].use_pass_combined = True
scn.render.layers['RenderLayer'].use_pass_vector = True
scn.render.layers['RenderLayer'].use_pass_z = False

# Render the animation
scn.render.filepath = os.path.join(target_dir, "file")
scn.render.image_settings.file_format = 'OPEN_EXR_MULTILAYER'
scn.render.image_settings.color_mode = 'RGBA'
scn.render.image_settings.use_preview = True

scn.frame_start = 1
scn.frame_end = max_frames


with open(os.path.join(target_dir,'used_exr_setup_config'), 'w') as f:
    f.write("target_img_w: {}\n".format(target_img_w))
    f.write("target_img_h: {}\n".format(target_img_h))
    f.write("camera_location: {}\n".format(camera_location))
    f.write("camera_rotation: {}\n".format(camera_rotation))
    f.write("light_location: {}\n".format(light_location))
    f.write("cube_color_1: {}\n".format(cube_color_1))
    f.write("cube_color_2: {}\n".format(cube_color_2))
    f.write("speed: {}\n".format(speed))
    f.write("path_points:\n")
    for point in path.data.splines[0].points:
        f.write("  {}\n".format(point.co))

bpy.ops.render.render(animation=True)

