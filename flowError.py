#!/usr/bin/env python

import numpy

def readFlo(file):
    with open(file, 'br') as f:
        tag = f.read(4)
        if tag != b'PIEH':
            print("File '{}' has wrong input type.".format(file))
            return None
        width = numpy.fromfile(f, dtype=numpy.int32, count=1)[0]
        height = numpy.fromfile(f, dtype=numpy.int32, count=1)[0]
        data = numpy.fromfile(f, dtype=numpy.float32)
    data.shape = (-1, 2) # two columns
    return data


def calculateAngleError(data1, data2):
    m12 = numpy.multiply(data1, data2)
    m11 = numpy.square(data1)
    m22 = numpy.square(data2)
    num = 1 + numpy.sum(m12, axis=1)
    den11 = 1 + numpy.sum(m11, axis=1)
    den22 = 1 + numpy.sum(m22, axis=1)
    denom = numpy.sqrt(den11 * den22)
    q = num/denom
    q[q>1] = 1.
    return numpy.arccos(q)


def calculateEndpointError(data1, data2):
    return numpy.sqrt(numpy.sum(numpy.square(data1-data2), axis=1))


def calculatePercentageOfOutliers(ee_orig, mask, error=3):
    """
    Input:
      - `ee_orig` - endpoint error per pixel
      - `mask` - binary image that defines the foreground pixel
      - `error` - threshold, which endpoint error are counted as outliers
    
    Output:
      Percentage of outliers over background, foreground and
      all ground truth pixels
    """
    total = mask.size
    count_fg = numpy.sum(mask)
    count_bg = total - count_fg
    ee_orig.shape = mask.shape
    outliers = ee_orig > error # error greater than 3 pixel
    
    all_avg = numpy.sum(outliers)/total * 100
    fg = outliers * mask
    fg_sum = numpy.sum(fg)
    fg_avg = numpy.sum(fg)/count_fg * 100
    
    bg = outliers * numpy.invert(mask)
    bg_sum = numpy.sum(bg)
    bg_avg = numpy.sum(bg)/count_bg * 100
    return bg_avg, fg_avg, all_avg


def compareFiles(file1, file2, mask=None):
    """
    Compute angle error and end point error between two flo files.
    
    Note: In general one of the input files is the ground truth.
    """
    data1 = readFlo(file1)
    if data1 is None:
        return
    data2 = readFlo(file2)
    if data2 is None:
        return

    endpointError = calculateEndpointError(data1, data2)
    ae = numpy.average(calculateAngleError(data1, data2))
    ee = numpy.average(endpointError)
    if mask is None:
        return ae, ee

    outliers_avg = calculatePercentageOfOutliers(endpointError, mask)

    return ae, ee, outliers_avg


def mask(imageA, imageB):
    """
    Return mask that indicates which pixel are different between
    the input images.
    """
    m = numpy.abs(imageA - imageB) > 0
    m = numpy.any(m, axis=2)
    return m

