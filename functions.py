import bpy
import os

if __name__ == "__main__":
    exit("You cannot run this file directly.")

# delete all objects
def delete_all():
    for action in bpy.data.actions:
        bpy.data.actions.remove(action, do_unlink=True)

    for armature in bpy.data.armatures:
        bpy.data.armatures.remove(armature, do_unlink=True)

    for brush in bpy.data.brushes:
        bpy.data.brushes.remove(brush, do_unlink=True)

    for cam in bpy.data.cameras:
        bpy.data.cameras.remove(cam, do_unlink=True)

    for grp in bpy.data.groups:
        bpy.data.groups.remove(grp, do_unlink=True)

    for img in bpy.data.images:
        bpy.data.images.remove(img, do_unlink=True)

    for lamp in bpy.data.lamps:
        bpy.data.lamps.remove(lamp, do_unlink=True)

    for material in bpy.data.materials:
        bpy.data.materials.remove(material, do_unlink=True)

    for mesh in bpy.data.meshes:
        bpy.data.meshes.remove(mesh, do_unlink=True)

    for object in bpy.data.objects:
        bpy.data.objects.remove(object, do_unlink=True)

    for scene in bpy.data.scenes:
        if scene.name != "Scene":
            bpy.data.scenes.remove(scene, do_unlink=True)

    for texture in bpy.data.textures:
        bpy.data.textures.remove(texture, do_unlink=True)

# add a camera fish eye
def add_fisheye_camera(cam_location, cam_rotation):
    bpy.ops.object.camera_add(view_align=True, location=cam_location, rotation=cam_rotation)
    bpy.context.object.data.type = 'PANO'
    bpy.context.object.data.cycles.panorama_type = 'FISHEYE_EQUIDISTANT'
    # bpy.context.object.data.cycles.fisheye_lens = 1 #2.7
    bpy.context.object.data.cycles.fisheye_fov = 3.14159
    bpy.context.object.data.sensor_width = 8.8
    bpy.context.object.data.sensor_height = 6.6
    return bpy.context.object

