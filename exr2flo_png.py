#!/usr/bin/env python3

import multiprocessing
import os
import sys
import OpenEXR
import Imath
import numpy
from PIL import Image, ImageMath

def create_flo(filename_without_ext):
    pt = Imath.PixelType(Imath.PixelType.FLOAT)

    data = OpenEXR.InputFile(os.path.join(path, filename_without_ext + ".exr"))
    dw = data.header()['dataWindow']
    size = (dw.max.x - dw.min.x + 1, dw.max.y - dw.min.y + 1)
    width, height = size

    # R: pixel displacement in X from current frame to previous frame
    # G: pixel displacement in Y from current frame to previous frame
    # B: pixel displacement in X from current frame to next frame
    # A: pixel displacement in Y from current frame to next frame
    # from https://blenderartists.org/forum/showthread.php?133266-RGB-Speed-Vector-Pass-in-external-compositing-apps

    # create flo file
    Xstr = data.channel('RenderLayer.Vector.Y', pt) # v
    Ystr = data.channel('RenderLayer.Vector.Z', pt) # u

    X = numpy.fromstring(Xstr, dtype=numpy.float32)
    Y = numpy.fromstring(Ystr, dtype=numpy.float32)

    X.shape = (width*height, 1)
    Y.shape = (width*height, 1)

    res = numpy.concatenate((Y, X), axis=1)

    res.shape = (height, width*2)
    TAG_STRING = b'PIEH'

    with open(os.path.join(ground_truth_dir, filename_without_ext + ".flo"), 'bw') as f:
        f.write(TAG_STRING)
        numpy.int32(width).tofile(f)
        numpy.int32(height).tofile(f)
        res.tofile(f)

    # create png file
    Rstr = data.channel('RenderLayer.Combined.R', pt)
    Gstr = data.channel('RenderLayer.Combined.G', pt)
    Bstr = data.channel('RenderLayer.Combined.B', pt)

    rgbf = [Image.frombytes("F", size, c) for c in (Rstr, Gstr, Bstr)]
    rgb8 = [ImageMath.eval("c * 255", c=im).convert("L") for im in rgbf]
    Image.merge("RGB", rgb8).save(os.path.join(image_dir, filename_without_ext + ".png"))


if __name__ == "__main__":
    if len(sys.argv) < 2:
        exit("You have to pass a directory.")

    path = os.path.abspath(sys.argv[1])
    if not os.path.isdir(path):
        exit("{} is not a directory".format(path))

    image_dir = os.path.join(path, "images")
    ground_truth_dir = os.path.join(path, "ground_truth")

    os.makedirs(image_dir, exist_ok=True)
    os.makedirs(ground_truth_dir, exist_ok=True)
    files = [f.rstrip(".exr") for f in os.listdir(path) if f[-4:] == ".exr" and os.path.isfile(os.path.join(path, f))]

    # create flo and png files
    pool = multiprocessing.Pool(4)
    pool.map(create_flo, files)

